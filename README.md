## rtksimpll - a PLL simulator
A simple PLL frequency synthesizer simulator written to avoid
the use of ADIsilPLL that's:

1. a closed source sw so: how does it works? Which assumption are
   taken? what kind of data encript the libraries? and so on...
   
2. a Win32 executable difficult to run on wine or other emulartor
   under linux
   
3. an annoying UI that require a lot of point and click when just
   numbers has to be input and read. More to this graph are difficult
   to use to take exact numbers
   
Too bad, I love ADI components!
   
## how to run
rtksimpll is just a python2 script that require to import:

- subprocess
- ConfigParser
- tempfile
- numpy
- re
- decimal
- scipy

that should already present on your system; otherwise pip is your friend.

Switches for help and template input file generation are provided.

## quick start
As a first, try:

    $ rtksimpll
	
	
	    PLL loop filter simulator
						  
	usage:
						  
	-h --help : this help
	-q --quiet: print only output data, no headings
	-o --output <filename>: write closed loop response and noise on <filename>
	-n --normalize: closed loop response normalize to 0dB at DC
	--template : print a template as input file
										  
and then:

    $ ./rtksimpll -q --template >template.pll
	
take a look at template.pll and than perform the real simulation with:

    $ ./rtksimpll template.pll -n -o template.dat

and at the end use your favorite data plotting sw to see the results;
with gnuplot I suggest 

    gnuplot> set logscale x
	gnuplot> set yrange [-150:-50]
	gnuplot> set y2range [-90:+10]
	gnuplot> plot 'template.dat' u 1:2 axes x1y2 w l, 'template.dat'\
		u 1:4 axes x1y1 w l


## loop filter calculation
Since v1.1 loop filter components calculation is possible specifying
a section named [req] in .pll file.
It's possible to specify different parameters upon which loop filter
network dimensioning can be done.
Mainly it's possible to specify bandwidth ('bw:' key) or dynamic
parameter 'fn:' and 'xi:' i.e. normal frequency and damping factor.
Template output (--template) show an example.
Loop filter network is shown in stdout or can be saved in a separate
file (using 'outfile:' key) or can be appended to .pll file using
'append:' key (without any parameter). In the latter case .pll file
needs to be hands edited.


## feedback
Let me know, rtksimpll is actually work in progress, write me, call
me:

    Lapo Pieri
    via delle Ortensie, 22
	50142 FIRENZE

	lapo@radioteknos.it
	ik5nax@radioteknos.it
	http://www.radioteknos.it/index_en.html
	http://www.radioteknos.it/ik5nax_en.html
	T. +39 055 706881 - +39 329 2599801

